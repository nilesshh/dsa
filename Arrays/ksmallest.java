import java.util.*;

public class ksmallest{
    public static void main(String args[]){
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int a[] = new int [n];
        for(int i=0;i<n;i++)
            a[i] = s.nextInt();
        int k = s.nextInt();

        PriorityQueue<Integer> pq = new PriorityQueue<>();
        for(int i = 0;i<n;i++){
            pq.add(a[i]);
        }
        for(int i = 1;i<k;i++)
            pq.poll();
        
        System.out.println(pq.peek());
    }
}