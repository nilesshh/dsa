#include <bits/stdc++.h>
using namespace std;
#define ll long long int

void printArray(ll a[], ll n){
    for(ll i = 0;i < n;i++)
        cout << a[i] << " ";
}

struct Pair
{
    ll min;
    ll max;
};
 
Pair getMinMax(ll arr[], ll n)
{
    struct Pair minmax;    
    ll i;     
    if (n == 1){
        minmax.max = arr[0];
        minmax.min = arr[0];    
        return minmax;
    }
     
    if (arr[0] > arr[1])
    {
        minmax.max = arr[0];
        minmax.min = arr[1];
    }
    else
    {
        minmax.max = arr[1];
        minmax.min = arr[0];
    }
     
    for(i = 2; i < n; i++)
    {
        if (arr[i] > minmax.max)    
            minmax.max = arr[i];
             
        else if (arr[i] < minmax.min)    
            minmax.min = arr[i];
    }
    return minmax;
}
 

int main(){
    ll n;
    cin >> n;
    ll a[n];
    for(ll i = 0;i < n;i++)
        cin >> a[i];
        
    struct Pair minmax = getMinMax(a, n);
    cout << "MIN = " << minmax.min << "\n";
    cout << "MAX = " << minmax.max << "\n";
    return 0;
}
