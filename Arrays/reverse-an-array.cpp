#include <bits/stdc++.h>
using namespace std;
#define ll long long int

void printArray(ll a[], ll n){
    for(ll i = 0;i < n;i++)
        cout << a[i] << " ";
}

int main(){
    ll n;
    cin >> n;
    ll a[n];
    for(ll i = 0;i < n;i++)
        cin >> a[i];
        
    ll left = 0, right = n - 1;
    while(left < right){
        ll temp = a[left];
        a[left] = a[right];
        a[right] = temp;
        left++;
        right--;
    }
    
    printArray(a,n);
    return 0;
}
