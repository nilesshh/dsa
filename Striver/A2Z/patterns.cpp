#include <bits/stdc++.h>
using namespace std;

void pattern1()
{
    for (int i = 0; i < 5; i++)
    {
        for (int j = 0; j <= i; j++)
        {
            cout << "* ";
        }
        cout << endl;
    }
}

void pattern2()
{
    for (int i = 0; i < 5; i++)
    {
        for (int j = 0; j <= i; j++)
        {
            cout << j + 1 << " ";
        }
        cout << endl;
    }
}

void pattern3()
{
    for (int i = 0; i < 5; i++)
    {
        for (int j = 0; j <= i; j++)
        {
            cout << i + 1 << " ";
        }
        cout << endl;
    }
}

void pattern4()
{
    for (int i = 0; i < 5; i++)
    {
        for (int j = 0; j < 5 - i; j++)
        {
            cout << "* ";
        }
        cout << endl;
    }
}

void pattern5()
{
    for (int i = 0; i < 5; i++)
    {
        for (int j = 0; j < 5 - i; j++)
        {
            cout << j + 1 << " ";
        }
        cout << endl;
    }
}

void pattern6(int n)
{
    for (int i = 0; i < n; i++)
    {
        // space
        for (int j = 0; j < n - i - 1; j++)
        {
            cout << " ";
        }
        // stars
        for (int j = 0; j < 2 * i + 1; j++)
        {
            cout << "*";
        }
        // space
        for (int j = 0; j < n - i - 1; j++)
        {
            cout << " ";
        }
        cout << endl;
    }
}

void pattern7(int n)
{
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < i; j++)
        {
            cout << " ";
        }
        // stars
        for (int j = 0; j < 2 * n - (2 * i + 1); j++)
        {
            cout << "*";
        }
        // space
        for (int j = 0; j < i; j++)
        {
            cout << " ";
        }
        cout << endl;
    }
}

void pattern8(int n)
{
    for (int i = 1; i <= 2 * n - 1; i++)
    {
        int stars = i;
        if (i > n)
            stars = 2 * n - i;
        for (int j = 1; j < stars; j++)
        {
            cout << "*";
        }
        cout << endl;
    }
}

void pattern9(int n)
{
    for (int i = 0; i < n; i++)
    {
        int start = 1;
        if (i % 2 == 0)
            start = 1;
        else
            start = 0;
        for (int j = 0; j <= i; j++)
        {
            cout << start;
            start = 1 - start;
        }
        cout << endl;
    }
}

void pattern10(int n){
    int space = 2*(n-1);
    for(int i = 1;i<=n;i++){
        for(int j = 1;j<=i;j++){
            cout << j;
        }

        for(int j=1;j<=space;j++){
            cout << " ";
        }

        for(int j = i;j>=1;j--){
            cout << j;
        }
        space -= 2;
        cout << endl;
    }
}

void pattern11(int n){
    int space = 0;
    for(int i=0;i<n;i++){
        for(int j=1; j<= n - i;j++){
            cout << "*";
        }
        for(int j=0; j<space;j++){
            cout << " ";
        }
        for(int j=1; j<= n - i;j++){
            cout << "*";
        }
        space +=2;
        cout << endl;
    }

    space = 2*n -2;
    for(int i=1;i<=n;i++){
        for(int j=1; j<=i;j++){
            cout << "*";
        }
        for(int j=0; j<space;j++){
            cout << " ";
        }
        for(int j=1; j<=i;j++){
            cout << "*";
        }
        space -=2;
        cout << endl;
    }
}

int main()
{
    pattern11(5);
}